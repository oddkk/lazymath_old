using System;
using System.Collections.Generic;

namespace mathtest
{
	public class Call
	{
		public string name;
		public Dictionary<string,Stmt> storage = new Dictionary<string,Stmt> ();

		public void SetStorage(string name,Stmt stmt)
		{
			if (storage.ContainsKey(name))
				storage[name] = stmt;
			else
				storage.Add(name,stmt);
		}
	}

	public static class State
	{
		public static Stack<double> stack = new Stack<double>();
		public static Dictionary<string,Stmt> storage = new Dictionary<string,Stmt>();
		public static Stack<Call> callStack = new Stack<Call>();

		public static void SetStorage(string name,Stmt stmt)
		{
			if (storage.ContainsKey(name))
				storage[name] = stmt;
			else
				storage.Add(name,stmt);
		}
	}
}
