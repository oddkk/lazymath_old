using System;
using System.Collections.Generic;

namespace mathtest
{
	public class Parser
	{
		Token[] tokens;
		public Queue<Stmt> program;
		int index = 0;
		
		string donotrefthis;

		void Next()
		{
			index++;
		}

		void Prev()
		{
			index--;
		}

		bool EOF
		{
			get {
				return index >= tokens.Length;
			}
		}

		Token Current
		{
			get {
				return tokens[index];
			}
		}

		public Parser(Token[] tokens)
		{
			this.tokens = tokens;
			program = new Queue<Stmt>();
		}


		public void ParseAll()
		{
			while (!EOF)
			{
				ParseStmt();
			}
		}

		public void ParseStmt()
		{
			if (IsWord)
			{
				if (CurrentWord == "print")
				{
					Next();
					program.Enqueue (new Print () { body = ParseExpr() });
				}
				else if (CurrentWord == "delete")
				{
					Next();
					Expect (TokenType.Word);
					program.Enqueue (new DelVar () { name = CurrentWord });
					Next();
				}
				else if (CurrentWord == "stack")
				{
					Next();
					Expect(TokenType.Word);
					program.Enqueue(new PrintStack() { name = CurrentWord });
					Next();
				}
				else
				{
					string name = CurrentWord;
					donotrefthis = name;
					Next ();
					Expect (TokenType.Symbol);
					if (CurrentSymbol == '(')
					{
						Next ();
						Queue<string> argNames = new Queue<string> ();
						while (!(IsSymbol && CurrentSymbol == ')'))
						{
							if (IsWord)
							{
								argNames.Enqueue (CurrentWord);
							}
							else
							{
								ExpectSymbol (',');
							}
							Next ();
						}
						Next ();
						ExpectSymbol ('=');
						Next ();
						program.Enqueue (new FuncDef () { name = name, args = argNames.ToArray(), body = ParseExpr() });
					}
					else if (CurrentSymbol == '=')
					{
						Next ();
						program.Enqueue (new VarDef () { name = name, body = ParseExpr() });
					}
					donotrefthis = "";
				}
			}
			else {
				throw new Exception ("Invalid input");
			}
		}

		public Expr ParseExpr(bool isArith = false,bool isParam = false)
		{
			Expr e = null;
			bool first = true;

			if (IsNumber) {
				first = false;
				e = new NumLit() { val = (double)Current.content };
				Next();
			}
			else if (IsWord) {
				first = false;
				string name = CurrentWord;
				Next();

				if (name == "sin")
				{
					ExpectSymbol ('(');
					Next ();
					e = new SinExpr () {  expr = ParseExpr(false,true) };
					ExpectSymbol (')');
					Next ();
				}
				else if (name == "pi")
				{
					e = new NumLit () { val = Math.PI };
				}
				else if (!EOF && IsSymbol && CurrentSymbol == '(')
				{
					Next ();
					Queue<Expr> args = new Queue<Expr>();
					while (!(IsSymbol && CurrentSymbol == ')'))
					{
						if (IsSymbol && CurrentSymbol == ',') Next();
						args.Enqueue(ParseExpr(false,true));
						/*
						if (!IsSymbol)
						{
						}
						else
						{
							ExpectSymbol(',');
							Next ();
						}*/
					}
					Next ();
					if(donotrefthis == name)
						throw new Exception("Cannot self reference, will cause infinite loop");
					e = new FuncCall() { name = name,args = args.ToArray() };
				}
				else
				{
					if(donotrefthis == name)
						throw new Exception("Cannot self reference, will cause infinite loop");
					e = new VarRef() { name = name };
				}
			}

			if (!EOF && IsSymbol && !isArith && CurrentSymbol != ')' && CurrentSymbol != ',')
			{
				Queue<Expr> output = new Queue<Expr>();
				Stack<char> operators = new Stack<char>();
				bool lastWasOp = false;
				bool lastWasParenClose = false;

				if(e != null)
					output.Enqueue(e);

				while (!EOF) {
					if (IsSymbol) {
						lastWasParenClose = false;
						if ((lastWasOp || first) && CurrentSymbol == '-') {
							output.Enqueue(new NumLit() { val = 0.0 });
						}
						if (CurrentSymbol == '(') {
							operators.Push(CurrentSymbol);
						}
						else if (CurrentSymbol == ')') {
							if (isParam) break;
							while (operators.Count > 0 && operators.Peek() != '(') {
								output.Enqueue(new OpExpr() { op = SymToOp(operators.Pop()) });
							}
							if (operators.Count == 0) throw new Exception("Unmatched parenthesis");
							operators.Pop();
							lastWasParenClose = true;
						}
						else if (CurrentSymbol == ',' && isParam) {
							break;
						}
						else {
							while (operators.Count > 0 && OpPri(operators.Peek()) >= OpPri(CurrentSymbol))
							{
								if (operators.Peek () == '(')
									break;
								output.Enqueue (new OpExpr () { op = SymToOp(operators.Pop()) });
							}
							operators.Push(CurrentSymbol);
						}
						lastWasOp = true;
						Next();
					}
					else
					{
						if (lastWasOp && !lastWasParenClose)
						{
							output.Enqueue(ParseExpr(true));
							lastWasOp = false;
						}
						else
							break;
					}
					first = false;
				}

				while (operators.Count > 0)
					output.Enqueue (new OpExpr () { op = SymToOp(operators.Pop()) });

				e = new ArithExpr() { body = output.ToArray() };
				//Console.WriteLine ("expression: {0}", e.ToString ());
			}
			return e;
		}

		public void Expect(TokenType t)
		{
			if (Current.type != t) throw new Exception(string.Format("Expected token of type {0}, got {1}",t.ToString(),Current.type.ToString()));
		}

		public void ExpectSymbol(char s)
		{
			if (!(IsSymbol && CurrentSymbol == s)) throw new Exception(string.Format("Expected symbol {0}, got {1}",s.ToString(),Current.content.ToString()));
		}

		Op SymToOp(char s)
		{
			switch (s)
			{
				case '+':
					return Op.Plus;
				case '-':
					return Op.Minus;
				case '*':
					return Op.Multiply;
				case '/':
					return Op.Divide;
				case '%':
					return Op.Modulo;
				case '^':
					return Op.Power;
			}
			throw new Exception(string.Format("Symbol {0} is not operator",s));
		}

		public byte OpPri(char v)
		{
			switch (v) {
				case '+':
				case '-':
				return 1;
				case '*':
				case '/':
				case '%':
				return 2;
				case '^':
				return 3;
				case '(':
				case ')':
				return 4;
			}
			return 0;
		}

		public bool IsWord
		{
			get {
				return Current.type == TokenType.Word;
			}
		}

		public string CurrentWord
		{
			get {
				return Current.content as string;
			}
		}

		public bool IsSymbol
		{
			get {
				return Current.type == TokenType.Symbol;
			}
		}

		public char CurrentSymbol
		{
			get {
				return (char)Current.content;
			}
		}

		public bool IsNumber
		{
			get {
				return Current.type == TokenType.Number;
			}
		}

		public double CurrentNumber
		{
			get {
				return (double)Current.content;
			}
		}
	}
}
