using System;
using System.IO;
using System.Text;

namespace mathtest
{
	class Program
	{
		static void Main(string[] args)
		{
			string text = "";
			bool showtrace = false;
			bool fail = false;
			Tokenizer t;
			Parser p;

			StringBuilder sb = new StringBuilder();

			foreach (var arg in args)
			{
				if (arg == "-s")
					showtrace = true;
				else if (arg == "-f")
					fail = true;
				else
					sb.AppendLine(File.ReadAllText(arg));
			}

			text = sb.ToString();

			if(text != "") {
				t = new Tokenizer(text); t.ScanAll();
				p = new Parser(t.tokens.ToArray()); p.ParseAll();

				foreach (var s in p.program) {
					s.Execute();
				}
			}

			string inp = "";

			Console.Write(">>> ");

			while ((inp = Console.ReadLine()) != "exit") {
				try
				{
					t = new Tokenizer(inp); t.ScanAll();
					p = new Parser(t.tokens.ToArray()); p.ParseAll();
				foreach (var s in p.program)
					s.Execute();
				} catch (Exception ex) {
					Console.WriteLine("Error: {0}", ex.Message);
					if(showtrace)
						Console.WriteLine("StackTrace: {0}", ex.StackTrace);
					if (fail)
						throw;
				}
				Console.Write(">>> ");
			}

		}
	}
}
