using System;
using System.Collections.Generic;

namespace mathtest
{
	public enum TokenType
	{
		Number,
		Word,
		Symbol
	}

	public class Token
	{
		public Token(TokenType type,object content)
		{
			this.type = type;
			this.content = content;
		}

		public TokenType type;
		public object content;
	}

	public class Tokenizer
	{
		private string input;
		private int index = 0;
		public List<Token> tokens = new List<Token>();

		public Tokenizer(string text)
		{
			this.input = text;
		}

		void Next()
		{
			++index;
		}

		char Current()
		{
			if (index >= input.Length) return (char)0;
			return input[index];
		}

		bool isSymbol()
		{
			return Current() == '(' ||
				   Current() == ')' ||
				   Current() == '+' ||
				   Current() == '-' ||
				   Current() == '*' ||
				   Current() == '/' ||
				   Current() == '=' ||
				   Current() == '^' ||
				   Current() == ',';
		}

		bool isLetter()
		{
			return char.IsLetter(Current());
		}

		bool isNumber()
		{
			return char.IsDigit(Current());
		}

		bool isDecimal()
		{
			return Current() == '.';
		}

		bool isWhitespace()
		{
			return Current() == ' ';
		}

		public void ScanAll()
		{
			index = 0;
			while (index < input.Length)
			{
				if (isLetter())
				{
					scanWord();
				}
				else if (isNumber() || isDecimal())
				{
					scanNumber();
				}
				else if (isSymbol())
				{
					scanSymbol();
				}
				else
				{
					Next();
				}
			}
		}

		void scanWord()
		{
			string word = "";

			while (isLetter() || isNumber())
			{
				word += Current();
				Next();
			}

			tokens.Add(new Token(TokenType.Word,word));
		}

		void scanNumber()
		{
			string number = "";

			while (isNumber() || isDecimal())
			{
				if (isNumber())
					number += Current();
				else
					number += '.';
				Next();
			}

			tokens.Add(new Token(TokenType.Number,double.Parse(number)));
		}

		void scanSymbol()
		{
			tokens.Add(new Token(TokenType.Symbol,Current()));
			Next();
		}
	}
}
