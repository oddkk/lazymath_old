using System;
using System.Linq;
using System.Collections.Generic;

namespace mathtest
{


	public abstract class Stmt
	{
		public abstract void Execute();
	}

	public abstract class Expr
	{
		public abstract void Eval();
	}

	public class FuncDef : Stmt
	{
		public string name;
		public string[] args;
		public Expr body;
		public Stmt parent = null;

		public override void Execute()
		{
			State.SetStorage(name,this);
		}

		public override string ToString()
		{
			return string.Format("{0}({1}): {2}",name,string.Join(",",args),body.ToString());
		}
	}

	public class VarDef : Stmt
	{
		public string name;
		public Expr body;
		public Stmt parent = null;

		public override void Execute()
		{
			State.SetStorage(name,this);
		}

		public override string ToString()
		{
			return string.Format("{0}: {1}",name,body.ToString());
		}
	}

	public class DelVar : Stmt
	{
		public string name;

		public override void Execute()
		{
			if (State.storage.ContainsKey(name))
			{
				State.storage.Remove(name);
			}
		}

		public override string ToString()
		{
			return string.Format("delete {0}",name);
		}
	}

	public class Print : Stmt
	{
		public Expr body;

		public override void Execute()
		{
			body.Eval();
			Console.WriteLine(State.stack.Pop().ToString());
		}

		public override string ToString()
		{
			return string.Format("print {0}",body.ToString());
		}
	}

	public class PrintStack : Stmt
	{
		public string name;

		public override void Execute()
		{
			if (State.storage.ContainsKey(name))
			{
				Console.WriteLine(State.storage[name].ToString());
			}
		}

		public override string ToString()
		{
			return string.Format("stack {0}",name);
		}
	}

	public class NumLit : Expr
	{
		public double val;

		public override void Eval()
		{
			State.stack.Push(val);
		}

		public override string ToString ()
		{
			return val.ToString ();
		}
	}

	public class VarRef : Expr
	{
		public string name;

		public override void Eval()
		{
			Stmt v = null;

			if (State.callStack.Peek ().storage.ContainsKey (name))
				v = State.callStack.Peek ().storage [name];
			else if (State.storage.ContainsKey (name))
				v = State.storage [name];
			else
				throw new Exception(string.Format("Undeclared variable {0}",name));

			if (v is FuncDef)
				throw new Exception (string.Format ("Function {0} referenced as variable", name));

			(v as VarDef).body.Eval ();
			/*
			string prefix = (State.callStack.Count > 0 ? State.callStack.Peek() + "_" : "");

			if (!State.storage.ContainsKey(prefix + name))
			{
				prefix = "";
				if(!State.storage.ContainsKey(prefix + name))
					throw new Exception(string.Format("Undeclared variable {0}",name));
			}
			(State.storage[prefix + name] as VarDef).body.Eval();
			*/
		}

		public override string ToString ()
		{
			return name;
		}
	}

	public class SinExpr : Expr
	{
		public Expr expr;

		public override void Eval()
		{
			expr.Eval ();
			State.stack.Push(Math.Sin(State.stack.Pop()));
		}

		public override string ToString ()
		{
			return expr.ToString ();
		}
	}

	public class FuncCall : Expr
	{
		public string name;
		public Expr[] args;

		public override void Eval()
		{
			/*
			string prefix = (State.callStack.Count > 0 ? State.callStack.Peek() + "_" : "");

			if (!State.storage.ContainsKey(prefix + name))
			{
				prefix = "";
				if(!State.storage.ContainsKey(prefix + name))
					throw new Exception(string.Format("Undeclared function {0}",name));
			}
			string pname = prefix + name;*/
			Stmt v = null;

			if (State.callStack.Peek ().storage.ContainsKey (name))
				v = State.callStack.Peek ().storage [name];
			else if (State.storage.ContainsKey (name))
				v = State.storage [name];
			else
				throw new Exception(string.Format("Undeclared variable {0}",name));

			if (v is VarDef)
			{
				var bf = (v as VarDef);
				var r = bf.body as VarRef;

				if(State.storage.ContainsKey(r.name)) {
					var f = State.storage[r.name] as FuncDef;

					if (f.args.Length != args.Length)
						throw new Exception(string.Format("Missmatched argument count in call to function {0}, expected {1} arguments: {2}",name,f.args.Length,string.Join(",",f.args)));
					
					State.callStack.Push(new Call() { name = f.name });
					for (int i = 0; i < args.Length; i++)
					{
						State.callStack.Peek().SetStorage(f.args[i], new VarDef(){ name = f.args[i], body = args[i] });
					}
					f.body.Eval();
					State.callStack.Pop();

					foreach (var fname in f.args)
						State.storage.Remove(fname);
				}
			}
			else if (v is FuncDef)
			{
				FuncDef f = v as FuncDef;

				if (f.args.Length != args.Length)
					throw new Exception(string.Format("Missmatched argument count in call to function {0}, expected {1} arguments: {2}",name,f.args.Length,string.Join(",",f.args)));

				State.callStack.Push(new Call() { name = f.name });
				for (int i = 0; i < args.Length; i++)
				{
					State.callStack.Peek().SetStorage(f.args[i], new VarDef(){ name = f.args[i], body = args[i] });
				}
				f.body.Eval();
				State.callStack.Pop();

				foreach (var fname in f.args)
					State.storage.Remove(fname);
			}
			else
				if (v == null)
					throw new Exception(string.Format("{0} is null",name));
				else
					throw new Exception(string.Format("Variable {0} referenced as function (type {1})",name,v.GetType().Name));
		}

		public override string ToString ()
		{
			return string.Format ("{0}({1})", name, string.Join(",",args.Select(x => x.ToString()).ToArray()));
		}
	}

	public class ExprStmt : Stmt
	{
		public Expr body;

		public override void Execute()
		{
			body.Eval();
		}
	}

	public class ArithExpr : Expr
	{
		public Expr[] body;

		public override void Eval()
		{
			foreach (var exp in body)
				exp.Eval();
		}

		public override string ToString()
		{
			return String.Join (",", string.Join(",",body.Select(x => x.ToString()).ToArray()));
		}
	}

	public enum Op
	{
		Plus,
		Minus,
		Multiply,
		Divide,
		Modulo,
		Power,
		OpenParen,
		CloseParen
	}

	public class OpExpr : Expr
	{
		public Op op;

		public override void Eval()
		{
			double b = State.stack.Pop();
			double a = State.stack.Pop();
			double c = 0;
			switch (op)
			{
				case Op.Plus:
					c = a + b;
					break;
				case Op.Minus:
					c = a - b;
					break;
				case Op.Multiply:
					c = a * b;
					break;
				case Op.Divide:
					c = a / b;
					break;
				case Op.Modulo:
					c = a % b;
					break;
				case Op.Power:
					c = Math.Pow(a,b);
					break;
			}
			State.stack.Push(c);
		}

		public override string ToString ()
		{
			return op.ToString ();
		}
	}
}
